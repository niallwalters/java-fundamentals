package com.training;

import com.training.person.Person;
import com.training.pets.Dragon;
import com.training.pets.Pet;

public class InterfaceMain {
    public static void main(String[] args) {
        Pet myPet = new Pet();

        Feedable[] thingsWeCanFeed = new Feedable[5];

        Person horace = new Person("Horace");
        horace.setName("Horace O'Brien");

        Dragon smaug = new Dragon("Smaug");
        smaug.breathesFire();

        thingsWeCanFeed[0] = horace;

        for(int i = 0; i < thingsWeCanFeed.length; i ++){
            if(thingsWeCanFeed[i] != null) {
                thingsWeCanFeed[i].feed();
            }

            if( thingsWeCanFeed[i] instanceof Dragon) {
                ((Dragon)thingsWeCanFeed[i]).breathesFire();
            }

        }
    }
}
