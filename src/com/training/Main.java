package com.training;

import com.training.pets.Dragon;
import com.training.pets.Pet;

public class Main {

    public static void main(String[] args) {

        Pet myPet = new Pet();
        System.out.println(Pet.getNumPets());
        myPet.setName("Isla");
        myPet.setNumLegs(4);

        System.out.println(myPet.getNumLegs());
        System.out.println(myPet.getName());
        myPet.feed();

        // ------------------------------------

        Dragon myDragon = new Dragon();
        myDragon.setName("Isla");


        Dragon usingNameConstructor = new Dragon("Derek");

        System.out.println(myDragon.getNumLegs());
        System.out.println(myDragon.getName());
        myDragon.feed("Goats");
        myDragon.breathesFire();

        Pet secondDragon = new Dragon();
        secondDragon.setName("Isla");

        System.out.println(secondDragon.getNumLegs());
        System.out.println(secondDragon.getName());
        secondDragon.feed();
        //secondDragon.breathesFire();
    }
}
