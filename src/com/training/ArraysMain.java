package com.training;

import com.training.pets.Dragon;
import com.training.pets.Pet;

public class ArraysMain{

    public static void main(String[] args) {

        int[] intArray = {5, 32, 64, 34, 77};

        Pet[] petArray = new Pet[10];

        //Put a pet in the array
        Pet firstPet = new Pet();
        firstPet.setName("First com.training.pets.Pet");

        petArray[0] = firstPet;

        //Put a dragon in the array
        Dragon firstDragon = new Dragon("Spyro");

        petArray[1] = firstDragon;

        for(int i = 0; i <= petArray.length;i++){

            if(petArray[i] != null){
                petArray[i].feed();
            }
        }

    }
}
