package com.training;

import com.training.pets.Dragon;

public class StringsMain {

    public static void main(String[] args) {

        String bigString = "Start: ";

        StringBuilder stringBuilder = new StringBuilder(bigString);

        for(int i = 0; i < 10; i++){
            stringBuilder.append(i);
        }

        System.out.println(stringBuilder);

        String testString = "hello" + " " + "world" + " " + "after" + " " + "lunch";
        System.out.println(testString);

        Dragon stringyDragon = new Dragon("Spyro");
        System.out.println("Dragon Created: " + stringyDragon.toString());

    }
}
