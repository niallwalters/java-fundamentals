package com.training.pets;

import com.training.Feedable;

public class Dragon extends Pet implements Feedable {

    public Dragon(){
        this.setNumLegs(4);
    }

    public Dragon(String name){
        this();
        this.setName(name);

    }
    public void feed(){
        System.out.println("Im feeding my dragon some locust");
    }

    public void feed(String food){
        System.out.println("Feed the com.training.pets.Dragon some " + food);
    }
    public void breathesFire(){
        System.out.println("My dragon breathes fire");
    }

    @Override
    public String toString(){
        return "This is a dragon named " + this.getName() + " with " + this.getNumLegs();
    }
}
