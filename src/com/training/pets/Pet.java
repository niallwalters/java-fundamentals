package com.training.pets;

public class Pet {

    private static int numPets = 0;

    private String name;
    private int numLegs;

    public Pet(){
        numPets++;
    }
    public void feed() {
        System.out.println("Im feeding a generic pet some generic food");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumLegs() {
        return numLegs;
    }

    public void setNumLegs(int numLegs) {
        this.numLegs = numLegs;
    }

    public static int getNumPets(){
        return numPets;
    }
}
